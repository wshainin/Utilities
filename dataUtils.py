import os
import xml.etree.cElementTree as cet
from subprocess import call
from scipy import misc
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import numpy as np
# Debug
from IPython import embed;

def get_file_list(path, extension='jpg'):
  print "Searching %s for %s files..." % (path, extension)
  path_list = []
  base = []
  #label_list = []

  for dir_path, file_path, file_names in os.walk(path):
      if dir_path.endswith('.AppleDouble'):
          continue
      for f in file_names:
          if f.lower().endswith((extension)) and not f.startswith('.'):
              full_path = os.path.join(dir_path, f)
              path_list.append(full_path)
              base.append(os.path.splitext(f)[0])
              #label_list.append(os.path.basename(dir_path))
  print "Found %d files." % len(path_list)

  return path_list, base

def call_batch_convert(image_list, dim_list, num_procs):
  print "Calling ImageMagick's convert with image list..."
  new_list = []
  resize_str = str(dim_list[0])+'x'+str(dim_list[1])
  temp_path = os.getcwd() + '/.tmp.txt'
  temp_file = open(temp_path,'w')

  for path in image_list:
      file_path, file_extension = os.path.splitext(path)
      print>>temp_file, file_path 
      new_list.append(file_path+'_'+resize_str+file_extension)
  temp_file.close()
  
  convert_call = ('<'+temp_path+' xargs -P'+str(num_procs)+' -I % convert %'+
      file_extension+' -resize '+resize_str+'^ -gravity center -crop '+
      resize_str+'+0+0 +repage %_'+resize_str+file_extension)
  
  call(convert_call, shell=True)
  os.remove(temp_path)

  return new_list

def call_batch_mirror(image_list, num_procs):
  print "Calling ImageMagick's convert with image list..."
  new_list = []
  resize_str = 'mirrored'
  temp_path = os.getcwd() + '/.tmp.txt'
  temp_file = open(temp_path,'w')

  for path in image_list:
      file_path, file_extension = os.path.splitext(path)
      print>>temp_file, file_path 
      new_list.append(file_path+'_'+resize_str+file_extension)
  temp_file.close()
  
  convert_call = ('<'+temp_path+' xargs -P'+str(num_procs)+' -I % convert %'+
      file_extension+' -flop %_'+resize_str+file_extension)

  call(convert_call, shell=True)
  os.remove(temp_path)
  embed()

  return new_list

def ILSVRC_xml_parse(xml_path):
    annotated_objects = []
    tree = cet.parse(xml_path)
    root = tree.getroot()
    for det_object in root.iter('object'):
        bounding_box = []
        bounding_box.append(det_object.find('name').text)
        bounding_box.append(int(det_object.find('bndbox/xmin').text))
        bounding_box.append(int(det_object.find('bndbox/xmax').text))
        bounding_box.append(int(det_object.find('bndbox/ymin').text))
        bounding_box.append(int(det_object.find('bndbox/ymax').text))
        annotated_objects.append(bounding_box)

    return annotated_objects

def display_annotation(image_path, annotated_objects):
  print "Drawing annotations..."
  img = misc.imread(image_path)
  plt.imshow(img)
  for det_object in annotated_objects:
      w = det_object[2] - det_object[1]
      h = det_object[4] - det_object[3]
      box = Rectangle((det_object[1], det_object[3]), w, h, 
                      fill=False, linewidth=2.0, ec='red')
      plt.gca().add_patch(box)
  plt.show()

def write_list_to_file(image_list, output_path):
  fpath = output_path + '/path_list.txt'
  out_file = open(fpath, 'w')
  print "Writing path list to %s..." % (fpath)
  for line in image_list:
      print>>out_file, line
  out_file.close()
