import sys
import os
import csv
from tqdm import tqdm


def read_dictionary(dict_f_path):
  labels = {}
  with open(dict_f_path) as csv_file:
    fields = ['label', 'text']
    reader = csv.DictReader(csv_file, fields)
    for row in tqdm(reader):
      labels[row['label']] = row['text']
  return labels

def write_list_to_file(label_list, out_file_path):
  out_file = open(out_file_path, 'w')
  for line in label_list:
    print>>out_file, line
  out_file.close()

def get_image_labels(label_f_path, return_dict=False):
  image_ids = []
  image_labels = []
  secondary_labels = []
  with open(label_f_path) as csv_file:
    fields = ['image_id', 'primary_label']
    reader = csv.DictReader(csv_file, fields, restkey='secondary_labels')
    reader.next() # Skip header (custom fields)

    for row in tqdm(reader):
      image_ids.append(row['image_id'])
      # NOTE: The following strips out the confidence value for the label
      if len(row) > 2:
        secondary_labels.append(row['secondary_labels'])
        #image_labels.append(row['secondary_labels'][0].split(':')[1])
      else:
        secondary_labels.append([])
      if row['primary_label'] is not None:
        # NOTE: to use 1st label only, comment image_labels append^ and
        # remove indentation of this if/else
        image_labels.append(row['primary_label'].split(':')[1])
      else:
        image_labels.append(None)
  if not (len(image_ids)==len(image_labels)==len(secondary_labels)):
    raise Exception("Lists are different lengths; indexes do not correspond.")
  if return_dict:
    return image_ids, image_labels, secondary_labels, dict(zip(image_ids, image_labels))
  else:
    return image_ids, image_labels, secondary_labels 

def get_all_labels(label_f_path, return_dict=False):
  image_ids = []
  image_labels = []
  with open(label_f_path) as csv_file:
    fields = ['image_id']
    reader = csv.DictReader(csv_file, fields, restkey='labels')
    reader.next() # Skip header (custom fields)

    for row in tqdm(reader):
      if len(row) < 2:
        continue
      image_ids.append(row['image_id'])
      labels_only = [l.split(':')[1] for l in row['labels']]
      image_labels.append(labels_only)
  if not (len(image_ids)==len(image_labels)):
    raise Exception("Lists are different lengths")
  return image_ids, image_labels

def get_image_urls(img_f_path):
  image_urls = {}
  with open(img_f_path) as csv_file:
    #fields = ['label', 'text']
    reader = csv.DictReader(csv_file)
    for row in tqdm(reader):
      if len(row) != 7:
        raise Exception('Encountered column mismatch in csv file')
        sys.exit()
      image_urls[row['ImageID']] = row['OriginalURL']
  return image_urls

def get_list_from_file(filename):
  line_list = []
  with open(filename, 'r') as in_file:
    line_list = [line[:-1] for line in in_file]
  return line_list

def get_labels_from_text(text_label_list, labels):
  label_keys = []
  for label, text in labels.iteritems():
    if text in text_label_list:
      label_keys.append(label)
  return label_keys
