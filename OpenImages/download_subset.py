import urllib
from joblib import Parallel, delayed
from OpenImages import read_dictionary, get_image_labels, get_image_urls, get_list_from_file, get_labels_from_text

#import argparse
#parser = argparse.ArgumentParser()
#parser.add_argument("input_path", type=str)

dict_f_path = '/home/wshainin/workspace/datasets/OpenImages/dict.csv'
label_f_path = '/home/wshainin/workspace/datasets/OpenImages/train/labels.csv'
img_f_path = '/home/wshainin/workspace/datasets/OpenImages/train/images.csv'
#label_f_path = '/home/wshainin/workspace/datasets/OpenImages/validation/machine-labels.csv'
#img_f_path = '/home/wshainin/workspace/datasets/OpenImages/validation/images.csv'

out_file_path = '/home/wshainin/workspace/datasets/OpenImages/labels.txt'
subset_file = '/home/wshainin/drive/_WORK/subset.txt'

print "Reading labels dictionary..."
labels = read_dictionary(dict_f_path)
print "Reading image labels..."
image_ids, image_labels, secondary_labels = get_image_labels(label_f_path)
print "Reading image URLs..."
image_urls = get_image_urls(img_f_path)

string_labels = get_list_from_file(subset_file)
label_keys = get_labels_from_text(string_labels, labels)
label_keys.remove('/m/015zzv') # Airplane runway, not fashion runway
download_indices = []
print "Finding URLs of desired labels..."
for label in tqdm(label_keys):
  download_indices = download_indices + [i for i, x in enumerate(image_labels) if x == label]
  remaining_indices = []
  print "Searching current files to determine remainin set to download..."
  for i in tqdm(range(len(download_indices))):
    if not os.path.isfile('/home/wshainin/DISK/datasets/OpenImages/512_Uncropped/'+image_ids[download_indices[i]]+'.jpg'):
      remaining_indices.append(download_indices[i])

#download_set = list(set(download_list))
def download(idx):
  urllib.urlretrieve(image_urls[image_ids[idx]], '/home/wshainin/MAG/datasets/OpenImages/' + image_ids[idx] + '.jpg') 

print "Collecting Garbage..."
del labels, label_keys, secondary_labels, image_labels, string_labels, download_indices
print "Setting up Parallel..."
Parallel(n_jobs=64, verbose=5)(delayed(download)(i) for i in tqdm(remaining_indices))

# NOTE Multiple runways exist
#   /m/015zzv  Airplane; Not in list?!?!?!
#   /m/03bxt6z Fashion
# Potentially more distractors? People without fashion/ fashion without people?
