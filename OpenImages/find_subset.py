import urllib
from tqdm import tqdm
from joblib import Parallel, delayed
from OpenImages import read_dictionary, get_all_labels, get_image_urls, get_list_from_file, get_labels_from_text

dict_f_path = '/home/wshainin/workspace/datasets/OpenImages/dict.csv'
subset_file = '/home/wshainin/drive/_WORK/subset.txt'
label_f_path = '/home/wshainin/workspace/datasets/OpenImages/train/labels.csv'
img_f_path = '/home/wshainin/workspace/datasets/OpenImages/train/images.csv'
#label_f_path = '/home/wshainin/workspace/datasets/OpenImages/validation/machine-labels.csv'
#img_f_path = '/home/wshainin/workspace/datasets/OpenImages/validation/images.csv'

print "Reading labels dictionary..."
labels = read_dictionary(dict_f_path)
print "Reading image labels..."
image_ids, image_labels = get_all_labels(label_f_path)
#print "Reading image URLs..."
#image_urls = get_image_urls(img_f_path)

string_labels = get_list_from_file(subset_file)
label_keys = get_labels_from_text(string_labels, labels)
label_keys.remove('/m/015zzv') # Airplane runway, not fashion runway
label_set = set(label_keys)

subset_ids = []
subset_labels = []
for idx, label_list in enumerate(tqdm(image_labels)):
  if len(label_set.intersection(set(label_list))) > 0:
    subset_ids.append(image_ids[idx])
    subset_labels.append(label_list)

labels[list(set(subset_labels[2]).intersection(label_set))[0]]







from IPython import embed;embed();raise Exception(':noitpecxE')

#print "Finding URLs of desired labels..."
#for label in tqdm(label_keys):
#  download_indices = download_indices + [i for i, x in enumerate(image_labels) if x == label]
#  remaining_indices = []
#  print "Searching current files to determine remainin set to download..."
#  for i in tqdm(range(len(download_indices))):
#    if not os.path.isfile('/home/wshainin/DISK/datasets/OpenImages/512_Uncropped/'+image_ids[download_indices[i]]+'.jpg'):
#      remaining_indices.append(download_indices[i])

# NOTE Multiple runways exist
#   /m/015zzv  Airplane; Not in list?!?!?!
#   /m/03bxt6z Fashion
# Potentially more distractors? People without fashion/ fashion without people?
